package com.example.demo.services;

import java.util.List;

import com.example.demo.entity.Item;
import com.example.demo.repository.ItemRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class ItemService {

    private final ItemRepo itemRepo;

    public void post(Item item) {
        itemRepo.save(item);
    }

    public Item get(Long id) {
        return itemRepo.findById(id).get();
    }

    public void delete(Item item) {
        itemRepo.delete(item);
    }

    public Item findbyphone(Long phone)
    {

        return itemRepo.findByPhone(phone);
    }

    public void updateItem(long id)
    {
        Item item = itemRepo.findById(id).get();
        item.setItem_name(item.getItem_name());
        itemRepo.save(item);

    }
}

