package com.example.demo.services;

import com.example.demo.entity.Customer;
import com.example.demo.repository.CustomerRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class CustomerService {

    private final CustomerRepo customerRepo;

    public void post(Customer customer) {
        customerRepo.save(customer);
    }

    public Customer get(Long id) {
        return customerRepo.findById(id).get();
    }

    public void delete(Customer customer) {
        customerRepo.delete(customer);
    }
    public void uppdateCustomer(long id,Customer customer)
    {

        Customer customer1 = customerRepo.findById(id).get();
        customer1.setName(customer.getName());
        customerRepo.save(customer1);


    }

}
