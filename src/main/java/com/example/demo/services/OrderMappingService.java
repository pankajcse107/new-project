package com.example.demo.services;


import java.util.Optional;

import com.example.demo.controller.ItemController;
import com.example.demo.entity.Item;
import com.example.demo.entity.Order;
import com.example.demo.entity.OrderMapper;
import com.example.demo.repository.ItemRepo;
import com.example.demo.repository.OrderMapperRepo;
import com.example.demo.repository.OrderRepo;
import com.sun.xml.internal.bind.annotation.OverrideAnnotationOf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class OrderMappingService {

    private final OrderMapperRepo orderMapperRepo;
    private final OrderRepo orderRepo;
    private final ItemRepo itemRepo;

    public void post(OrderMapper orderMapper) {
        orderMapperRepo.save(orderMapper);
    }
    public OrderMapper mapOrder(OrderMapper orderMapper)
    {
        Optional<Item> item = itemRepo.findById(orderMapper.getItem_id());
        Optional<Order> order = orderRepo.findById(orderMapper.getOrderPlaceId());
        if(orderMapper.getQuantity()<5)
        {
            OrderMapper cart1 = orderMapperRepo.save(orderMapper);
            return cart1;


        }
        return null;

    }
    public OrderMapper getordermap(Long userId) {
        return orderMapperRepo.findById(userId).get();
    }



   /* public OrderMapper getOrder(Long order_id) {
        return orderMapperRepo.findById(order_id).get();
    }*/


}
