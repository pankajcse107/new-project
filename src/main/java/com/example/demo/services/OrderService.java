package com.example.demo.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.naming.ldap.PagedResultsControl;

import com.example.demo.entity.Customer;
import com.example.demo.entity.Item;
import com.example.demo.entity.Order;
import com.example.demo.entity.OrderMapper;
import com.example.demo.repository.CustomerRepo;
import com.example.demo.repository.ItemRepo;
import com.example.demo.repository.OrderMapperRepo;
import com.example.demo.repository.OrderRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor_ = {@Autowired} )
public class OrderService {

    private final OrderRepo orderRepo;
    private final ItemRepo itemRepo;
    private final OrderMapperRepo orderMapperRepo;
    public Order createOrder(Order order)
    {
        order = orderRepo.save(order);
        float total = 0;
        int count = 0;
        for(OrderMapper m :order.getOrderMappers())
        {
            Item item = itemRepo.findById(m.getItem_id()).get();
            total = (float) (m.getQuantity()*item.getItem_price()+total);
            m.setOrderPlaceId(order.getOrder_id());
          count++;

        }
        orderRepo.save(order);
        orderMapperRepo.saveAll(order.getOrderMappers());
        return order;

    }

    public Order find(Long id)
    {

        Order order = orderRepo.findById(id).get();
        List<OrderMapper> orderMappers = orderMapperRepo.findByOrderPlaceId(id);
        order.setOrderMappers(orderMappers);
        return order;
    }


}
