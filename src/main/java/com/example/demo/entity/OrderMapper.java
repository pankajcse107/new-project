package com.example.demo.entity;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "ordermap")
public class OrderMapper {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ordermapid;
    private Long orderPlaceId;
    private Long item_id;
    private int quantity;



}
