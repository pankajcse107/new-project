package com.example.demo.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;



import lombok.Data;

@Data
@Entity
@Table(name = "newOrder")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long order_id;
    Long phone;
    BigDecimal totalAmount;

    @Transient
    List<OrderMapper> orderMappers;

    @Transient
     List<Customer> customers;

}
