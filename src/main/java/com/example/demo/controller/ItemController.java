package com.example.demo.controller;

import com.example.demo.entity.Item;
import com.example.demo.services.ItemService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;


@RestController
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
@RequestMapping(value = "value")
public class ItemController {

    private final ItemService itemService;

    @PostMapping
    public void post(Item item) {
        itemService.post(item);
    }

    //customerphone

    //put
    @PutMapping
    public void updateitem(Long id) {
        itemService.updateItem(id);
    }

    @GetMapping
    public Item get(Long phone) {
        return itemService.findbyphone(phone);
    }

    @DeleteMapping
    public void delete(Item item) {
        itemService.delete(item);
    }

}
