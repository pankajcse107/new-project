package com.example.demo.controller;

import java.beans.Transient;

import com.example.demo.entity.Customer;
import com.example.demo.services.CustomerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
@RequestMapping(value = "customer")
public class CustomerController {

    private final CustomerService customerService;

    @PostMapping
    public void post(Customer customer) {
        customerService.post(customer);
    }

    //phone number
    @PutMapping
    public void update(@RequestParam Long id, @RequestBody Customer customer) {
        customerService.uppdateCustomer(id, customer);
    }


    @GetMapping
    public Customer get(Long id) {
        return customerService.get(id);
    }

    @DeleteMapping
    public void delete(Customer customer) {
        customerService.delete(customer);
    }


}
