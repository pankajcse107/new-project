package com.example.demo.controller;

import java.util.Optional;

import com.example.demo.entity.Customer;
import com.example.demo.entity.Order;
import com.example.demo.repository.ItemRepo;
import com.example.demo.services.ItemService;
import com.example.demo.services.OrderService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;


@RestController
@RequiredArgsConstructor(onConstructor_ = {@Autowired} )
@RequestMapping(value = "order")
public class OrderController {

    private final OrderService orderService;


    @PostMapping
     public void post(Order order)
    {
        orderService.createOrder(order);
    }

    //phone

    @GetMapping
    public Order get(Long phone)
    {

       return orderService.find(phone);
    }


   /*@PutMapping
    public void update(Long id)
    {
       orderService.update(id);
    }*/

}
