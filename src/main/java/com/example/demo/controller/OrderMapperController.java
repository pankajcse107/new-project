package com.example.demo.controller;

import com.example.demo.entity.OrderMapper;
import com.example.demo.services.OrderMappingService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor(onConstructor_ = {@Autowired} )
@RequestMapping(value = "ordermap")
public class OrderMapperController {
    private final OrderMappingService orderMappingService;


    @PostMapping
    public void post(OrderMapper orderMapper)
    {
         orderMappingService.mapOrder(orderMapper);
    }

    @GetMapping
    public OrderMapper get(Long id)
    {
        return orderMappingService.getordermap(id);
    }



}
