package com.example.demo.repository;

import java.awt.List;

import com.example.demo.entity.Item;
import com.example.demo.entity.Order;

import org.springframework.data.repository.CrudRepository;

public interface OrderRepo extends CrudRepository<Order,Long> {

    Order findByPhone(Long phoneNumber);

}
