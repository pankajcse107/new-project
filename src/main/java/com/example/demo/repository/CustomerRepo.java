package com.example.demo.repository;

import com.example.demo.entity.Customer;

import org.springframework.data.repository.CrudRepository;

public interface CustomerRepo extends CrudRepository<Customer,Long> {

    Customer findByPhone(Long phone);
}
