package com.example.demo.repository;

import java.util.List;

import com.example.demo.entity.OrderMapper;

import org.springframework.data.repository.CrudRepository;

public interface OrderMapperRepo extends CrudRepository<OrderMapper,Long> {
    List<OrderMapper> findByOrderPlaceId(Long orderPlacedId);

}
