package com.example.demo.repository;

import com.example.demo.entity.Item;

import org.springframework.data.repository.CrudRepository;

public interface ItemRepo  extends CrudRepository<Item,Long> {

    Item findByPhone(Long phone);
}
